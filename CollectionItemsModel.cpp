
#include "CollectionItemsModel.hpp"


CollectionItemsModel::CollectionItemsModel (const std::shared_ptr<App> &app, QObject *parent)
  : QAbstractListModel (parent)
  , _app (app)
{
}

QHash<int, QByteArray> CollectionItemsModel::roleNames () const
{
    QHash<int, QByteArray> roles;
    roles[(int)Roles::Title] = "title";
    roles[(int)Roles::Preview] = "preview";
    roles[(int)Roles::Tags] = "tags";
    roles[(int)Roles::Body] = "body";
    
    return roles;
}

QVariant CollectionItemsModel::data (const QModelIndex &index, int role) const
{
  QVariant result;

  if (index.isValid () && static_cast<std::size_t> (index.row ()) < _items.size ())
    {
      loadItem (index.row ());
      
      const auto &item = *_items[index.row ()];
          
      switch (role)
        {
        case static_cast<int> (Roles::Title):
          result = item.title;
          break;
        case static_cast<int> (Roles::Preview):
          result = item.preview;
          break;
        case static_cast<int> (Roles::Tags):
          result = item.tags;
          break;
        case static_cast<int> (Roles::Body):
          result = loadBody (index.row ());
          break;
        default:
          // empty
          break;
        }
    }
    
  return result;
}

int CollectionItemsModel::rowCount (const QModelIndex &parent) const
{
  if (parent.isValid ())
    {
      return 0;
    }
  else
    {
      return static_cast<int> (_items.size ());
    }  
}

DbId CollectionItemsModel::getItem (int index)
{
  return _ids[index];
}

void CollectionItemsModel::setItems (::rust::Vec<DbId> &&items)
{
  beginResetModel ();
  
  _ids = items;
  
  _items.clear ();
  _items.resize (_ids.size ());

  endResetModel ();
}

void CollectionItemsModel::loadItem (std::size_t index) const
{
  if (!_items[index])
    {
      auto dbitem = kb2::load_collection_item_preview (_app->runtime (), _app->db (), _ids[index], 100);
      
      CollectionItem item;
      item.title = QString::fromUtf8 (dbitem->get_title ().c_str ());
      item.preview = QString::fromUtf8 (dbitem->get_body ().c_str ()).replace ("\n", " ");

      for (const auto tagid : dbitem->get_tags ())
        {
          auto tag = _app->tags ()->get_tag (tagid);
          item.tags.push_back (QString::fromUtf8 (tag.c_str ()));
        }
      item.tags.sort ();

      _items[index].emplace (std::move (item));
    }
}

QString CollectionItemsModel::loadBody (std::size_t index) const
{
  return QString::fromUtf8 (kb2::load_collection_item_body (_app->runtime (), _app->db (), _ids[index]).c_str ());
}
