
#include "ViewController.hpp"


void ViewController::displayError (const char *err)
{
  QQmlComponent component (app ()->engine (), QUrl (QStringLiteral ("qrc:/error.qml")));
  auto dialog = component.create ();

  dialog->setParent (view ());
  dialog->setProperty ("errorText", err ? QString::fromUtf8 (err) : QVariant ());

  if (auto it = item ())
    {
      dialog->setProperty ("parentItem", QVariant::fromValue (it));
    }
  else
    {
      dialog->setProperty ("modality", "ApplicationModal");
    }

  QMetaObject::invokeMethod (dialog, "open");
}
