import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("kb2-qt5", "main window title")

    property alias itemsModel: items.model
    property bool updating: false
    property alias backendItemBar: backendItemBar

    signal search(string query, int sorting)
    signal collections()
    signal update()
    signal config()
    signal itemChanged(int index)

    header: ToolBar {
        RowLayout {
            ToolButton { text: qsTr("Collections", "main window collections button"); onClicked: root.collections() }
            ToolButton { text: qsTr("Update", "main window update button"); onClicked: root.update() }
            ToolButton { text: qsTr("Config", "main window config button"); onClicked: root.config() }
        }
    }

    Component {
        id: itemDelegate

        Rectangle {
            id: itemRoot
            width: parent ? parent.width : 0
            height: column.height
            color: ListView.isCurrentItem ? palette.button : "transparent"
            clip: true

            required property string title
            required property string preview
            required property var tags
            required property int index
            required property var model

            ColumnLayout {
                id: column
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 2

                Rectangle { height: 2; Layout.fillWidth: true }
                Text { text: itemRoot.title; Layout.fillWidth: true }
                Text {
                    text: itemRoot.preview
                    maximumLineCount: 2
                    elide: Text.ElideRight
                    wrapMode: Text.WrapAnywhere
                    Layout.fillWidth: true
                }
                RowLayout {
                    spacing: 4
                    Repeater {
                        model: tags
                        delegate: tagDelegate
                    }
                }
                Rectangle { height: 2; Layout.fillWidth: true }
            }

            MouseArea {
                anchors.fill: itemRoot
                onClicked: itemRoot.ListView.view.currentIndex = itemRoot.index
            }
        }
    }

    Component {
        id: tagDelegate

        Rectangle {
            id: tagRoot
            width: tagText.width + 4
            height: tagText.height + 4
            border.width: 1

            required property string modelData

            Text {
                id: tagText
                text: tagRoot.modelData
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 2
            }
        }
    }

    SplitView {
        id: contentRoot
        anchors.fill: parent

        ColumnLayout {
            SplitView.preferredWidth: 200

            TextInput {
                id: filter
                selectByMouse: true
                Layout.fillWidth: true
                onAccepted: search.clicked()
            }

            ComboBox {
                id: sorting
                Layout.fillWidth: true

                // TODO: A proper enum maybe.
                model: [
                    qsTr("Rank asc", "main window rank asc sorting"),
                    qsTr("Rank desc", "main window rank desc sorting"),
                    qsTr("Date asc", "main window date asc sorting"),
                    qsTr("Date desc", "main window date decs sorting")
                ]
            }

            Button {
                id: search
                text: qsTr("Search", "main window search button")
                Layout.fillWidth: true
                onClicked: root.search(filter.text.toString(), 1 + sorting.currentIndex)
            }

            ListView {
                id: items
                delegate: itemDelegate
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true
                reuseItems: true

                onCurrentIndexChanged: root.itemChanged(items.currentIndex)

                ScrollBar.vertical: ScrollBar { }
            }
        }

        ColumnLayout {
            SplitView.fillWidth: true
            SplitView.fillHeight: true

            Item {
                id: backendItemBar
                Layout.fillWidth: true
                Layout.preferredHeight: children[0] ? children[0].height : 0
            }

            ScrollView {
                clip: true
                Layout.fillWidth: true
                Layout.fillHeight: true

                TextEdit {
                    id: body
                    readOnly: true
                    selectByKeyboard: true
                    selectByMouse: true
                    text: items.currentItem ? items.currentItem.model.body : ""
                }
            }
        }
    }

    BusyIndicator {
        id: busyIndicator
        anchors.centerIn: parent
        running: false
    }

    onUpdatingChanged: function () {
        root.header.enabled = !root.updating;
        contentRoot.enabled = !root.updating;
        search.enabled = !root.updating;
        items.enabled = !root.updating;
        busyIndicator.running = root.updating;
    }

    onClosing: function (e) {
        if (e.accepted) {
          e.accepted = !root.updating;
        }
    }
}
