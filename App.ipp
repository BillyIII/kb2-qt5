
inline QQmlApplicationEngine* App::engine () const
{
  return _engine;
}

inline const ::rust::Box<kb2::Runtime>& App::runtime () const
{
  return _runtime;
}

inline const ::rust::Box<kb2::Db>& App::db () const
{
  return _db;
}

inline const ::rust::Box<kb2::Tags>& App::tags () const
{
  return _tags;
}

inline ::rust::Box<kb2::Tags>& App::tags ()
{
  return _tags;
}

inline const ::rust::Box<kb2::CollectionBackends>& App::backends () const
{
  return _backends;
}

inline const App::UIBackends& App::uiBackends () const
{
  return _uiBackends;
}

inline CollectionBackendUI& App::uiBackend (QString name)
{
  auto b = _uiBackends.find (name);
  if (b == _uiBackends.end ())
    {
      throw std::runtime_error ("Invalid backend code");
    }
  return *b->second;
}
