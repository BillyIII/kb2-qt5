#ifndef COLLECTIONBACKENDUI_HPP
#define COLLECTIONBACKENDUI_HPP

#include <memory>

#include "Common.hpp"


struct CollectionBackendUI
{
  virtual class ViewController* createCollection (const std::shared_ptr<class App> &app) = 0;
  virtual class ViewController* modifyCollection (const std::shared_ptr<class App> &app, DbId id) = 0;
  virtual class ViewController* itemBar (const std::shared_ptr<class App> &app, DbId id) = 0;
};

#endif // COLLECTIONBACKENDUI_HPP
