#ifndef MAINVIEWCONTROLLER_HPP
#define MAINVIEWCONTROLLER_HPP

#include <memory>
#include <optional>
#include <string>

#include <QObject>
#include <QQuickWindow>
#include <QThread>

#include "ViewController.hpp"
#include "CollectionItemsModel.hpp"


class CollectionsUpdateThread : public QThread
{
  Q_OBJECT
  
public:
  CollectionsUpdateThread (const std::shared_ptr<App> &app, QObject *parent = nullptr);
  
  virtual void run () override;

  const std::optional<std::string> error () const;
  
signals:
    void updateDone ();

private:
  std::shared_ptr<App> _app;
  std::optional<std::string> _error;
};

class MainViewController : public ViewController
{
  Q_OBJECT

public:
  MainViewController (const std::shared_ptr<App> &app, CollectionItemsModel *model, QObject *view,
                      QObject *parent = nullptr);

private slots:
  void onSearch (QString query, int sorting);
  void onCollections ();
  void onUpdate ();
  void onUpdateDone ();
  void onConfig ();
  void onItemChanged (int index);

private:
  CollectionItemsModel *_model;
  CollectionsUpdateThread *_updateThread = nullptr;
  QQuickItem *_backendItemBarView = nullptr;
};

#endif // MAINVIEWCONTROLLER_HPP
