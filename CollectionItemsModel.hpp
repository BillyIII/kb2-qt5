#ifndef COLLECTIONITEMSMODEL_HPP
#define COLLECTIONITEMSMODEL_HPP

#include <vector>
#include <optional>
#include <memory>

#include <QAbstractListModel>

#include "Common.hpp"
#include "App.hpp"


struct CollectionItem
{
  QString title;
  QString preview;
  QStringList tags;

  CollectionItem () = default;
  CollectionItem (const CollectionItem &other) = delete;
  CollectionItem (CollectionItem &&other) = default;
};

class CollectionItemsModel : public QAbstractListModel
{
  Q_OBJECT
  
public:
  enum class Roles : int
    {
      Title = Qt::UserRole,
      Preview,
      Tags,
      Body
    };

  CollectionItemsModel (const std::shared_ptr<App> &app, QObject *parent = 0);

  virtual QHash<int, QByteArray> roleNames () const override;
  virtual QVariant data (const QModelIndex &index, int role) const override;
  virtual int rowCount (const QModelIndex &parent) const override;

  DbId getItem (int index);
  void setItems (::rust::Vec<DbId> &&items);
   
private:
  std::shared_ptr<App> _app;
  ::rust::Vec<DbId> _ids;
  // :-(
  mutable std::vector<std::optional<CollectionItem>> _items;

  void loadItem (std::size_t index) const;
  QString loadBody (std::size_t index) const;
};

#endif // COLLECTIONITEMSMODEL_HPP
