import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3


ToolBar {
    id: root

    signal open()
    signal mailcap()

    RowLayout {
        ToolButton { text: qsTr("Open", "files item bar - open button"); onClicked: root.open() }
        ToolButton { text: qsTr("Mailcap", "files item bar - mailcap button"); onClicked: root.mailcap() }
    }
}
