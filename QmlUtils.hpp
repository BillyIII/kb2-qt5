#ifndef QMLUTILS_H
#define QMLUTILS_H

#include <QObject>
#include <QUrl>


class QmlUtils : public QObject
{
  Q_OBJECT

public:
  explicit QmlUtils (QObject *parent = nullptr);

  Q_INVOKABLE QUrl urlFromLocalPath (QString path);
  Q_INVOKABLE QString localPathFromUrl (QUrl url);

signals:
};

#endif // QMLUTILS_H
