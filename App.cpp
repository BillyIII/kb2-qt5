
#include "App.hpp"
#include "FilesCollectionBackendUI.hpp"


App::App (QQmlApplicationEngine *engine, const char *db)
  : _engine (engine)
  , _runtime (kb2::runtime_create ())
  , _db (kb2::db_create (db))
  , _tags (kb2::load_tags (_runtime, _db))
  , _backends (kb2::load_collection_backends ())
{
  _uiBackends.insert (UIBackends::value_type ("files", new FilesCollectionBackendUI ()));
}
