#ifndef APP_HPP
#define APP_HPP

#include <memory>
#include <unordered_map>
#include <stdexcept>

#include <QQmlApplicationEngine>

#include "../kb2_cxx/target/kb2.hpp"
#include "CollectionBackendUI.hpp"


class App
{
public:
  using UIBackends = std::unordered_map<QString, std::unique_ptr<CollectionBackendUI>>;
  
  App (QQmlApplicationEngine *engine, const char *db);

  QQmlApplicationEngine* engine () const;
  const ::rust::Box<kb2::Runtime>& runtime () const;
  const ::rust::Box<kb2::Db>& db () const;
  const ::rust::Box<kb2::Tags>& tags () const;
  ::rust::Box<kb2::Tags>& tags ();
  const ::rust::Box<kb2::CollectionBackends>& backends () const;
  const UIBackends& uiBackends () const;
  CollectionBackendUI& uiBackend (QString name);

private:
  QQmlApplicationEngine *_engine;
  ::rust::Box<kb2::Runtime> _runtime;
  ::rust::Box<kb2::Db> _db;
  ::rust::Box<kb2::Tags> _tags;
  ::rust::Box<kb2::CollectionBackends> _backends;
  UIBackends _uiBackends;
};

#include "App.ipp"

#endif // APP_HPP
