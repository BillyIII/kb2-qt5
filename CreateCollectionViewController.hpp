#ifndef CREATECOLLECTIONVIEWCONTROLLER_HPP
#define CREATECOLLECTIONVIEWCONTROLLER_HPP

#include <QQuickItem>

#include "ViewController.hpp"


class CreateCollectionViewController : public ViewController
{
  Q_OBJECT
  
public:
  CreateCollectionViewController (const std::shared_ptr<App> &app, QObject *view,
                                  QObject *parent = nullptr);

signals:
  void accepted (DbId id);
  void rejected ();
                                                            
private slots:
  void onBackendChanged (QString name);
  void onBackendAccepted (DbId id);
  void onBackendRejected ();

private:
  QQuickItem *_backendView = nullptr;
};

#endif // CREATECOLLECTIONVIEWCONTROLLER_HPP
