import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import Qt.labs.platform 1.1
import Kb2 1.0


Item {
    id: root
    anchors.fill: parent

    property alias path: pathText.text
    property alias maxTextSize: maxTextSizeText.text
    property alias maxContentSize: maxContentSizeText.text

    FolderDialog {
        id: pathDialog
        modality: Qt.WindowModal

        onVisibleChanged: pathDialog.currentFolder = Utils.urlFromLocalPath(pathText.text)
    }

    ScrollView {
        id: fieldsScroll
        anchors.fill: parent
        contentWidth: fieldsScroll.availableWidth

        GridLayout {
            columns: 2

            Text { text: qsTr("Path", "edit files coll. panel - collection path") }
            RowLayout {
                TextInput {
                    id: pathText
                    Layout.fillWidth: true
                    verticalAlignment: TextInput.AlignVCenter
                    text: Utils.localPathFromUrl(pathDialog.folder)
                }
                Button { text: qsTr("Browse", "edit files coll. panel - collection path browse button"); onClicked: pathDialog.open() }
            }

            Text { text: qsTr("Max text size", "edit files coll. panel - max text size") }
            TextInput { id: maxTextSizeText; Layout.fillWidth: true; validator: IntValidator }

            Text { text: qsTr("Max content size", "edit files coll. panel - max content size") }
            TextInput { id: maxContentSizeText; Layout.fillWidth: true; validator: IntValidator }
        }
    }
}
