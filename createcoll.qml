import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

Window {
    id: root
    width: 640
    height: 480
    title: qsTr("Create collection", "new coll. window title")

    property alias backendsModel: backends.model
    property alias backendView: backendView

    signal backendChanged(string name)

    signal accepted()
    signal rejected()

    Component {
        id: backendDelegate
        Rectangle {
            id: backendRoot
            width: backendName.width
            height: backendName.height
            color: ListView.isCurrentItem ? "grey" : "transparent"

            required property int index
            required property var modelData

            Text {
                id: backendName
                width: parent.ListView.view.width
                clip: true

                Text { width: parent.width; text: backendRoot.modelData }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: parent.ListView.view.currentIndex = parent.index
            }
        }
    }

    SplitView {
        anchors.fill: parent

        ListView {
            id: backends
            delegate: backendDelegate
            SplitView.preferredWidth: 200
            onCurrentIndexChanged: root.backendChanged(backends.currentItem.modelData)
        }

        ColumnLayout {
            Item {
                id: backendView
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            RowLayout {
                layoutDirection: Qt.RightToLeft

                Button { text: qsTr("Cancel", "new coll. panel - cancel button"); onClicked: root.rejected() }
                Button { text: qsTr("Ok", "new coll. panel - ok button"); onClicked: root.accepted() }
            }
        }
    }
}
