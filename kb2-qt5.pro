QT += quick widgets quickcontrols2

CONFIG += c++20 debug

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

PRECOMPILED_HEADER = pch.hpp

SOURCES += \
        ../kb2_cxx/target/kb2.cpp \
        App.cpp \
        CollectionItemsModel.cpp \
        CollectionsModel.cpp \
        CollectionsViewController.cpp \
        Config.cpp \
        ConfigViewController.cpp \
        CreateCollectionViewController.cpp \
        FilesCollectionBackendUI.cpp \
        MainViewController.cpp \
        QmlUtils.cpp \
        ViewController.cpp \
        main.cpp

RESOURCES += qml.qrc

TRANSLATIONS = default.ts

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ../kb2_cxx/target/kb2.hpp \
    App.hpp \
    App.ipp \
    CollectionBackendUI.hpp \
    CollectionItemsModel.hpp \
    CollectionsModel.hpp \
    CollectionsViewController.hpp \
    Common.hpp \
    Config.hpp \
    ConfigViewController.hpp \
    CreateCollectionViewController.hpp \
    FilesCollectionBackendUI.hpp \
    MainViewController.hpp \
    QmlUtils.hpp \
    ViewController.hpp \
    ViewController.ipp \
    pch.hpp

unix:!macx:CONFIG(debug, debug|release): LIBS += -L$$PWD/../kb2_cxx/target/debug/ -lkb2_cxx -lsqlite3 -ldl
else:unix:!macx:CONFIG(release, debug|release): LIBS += -L$$PWD/../kb2_cxx/target/release/ -lkb2_cxx -lsqlite3 -ldl

INCLUDEPATH += $$PWD/../kb2_cxx/target/debug
DEPENDPATH += $$PWD/../kb2_cxx/target/debug

unix:!macx::CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../kb2_cxx/target/debug/libkb2_cxx.a
else:unix:!macx::CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../kb2_cxx/target/release/libkb2_cxx.a
