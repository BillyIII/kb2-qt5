
#include <QQmlComponent>

#include "CollectionsViewController.hpp"
#include "CreateCollectionViewController.hpp"


CollectionsViewController::CollectionsViewController (const std::shared_ptr<App> &app_,
                                                      CollectionsModel *model, QObject *view_,
                                                      QObject *parent)
  : ViewController (app_, view_, parent)
  , _model (model)
{
  QObject::connect (view (), SIGNAL (collectionIndexChanged(int)), this, SLOT (onCollectionIndexChanged(int)));
  QObject::connect (view (), SIGNAL (createCollection()), this, SLOT (onCreateCollection()));
  QObject::connect (view (), SIGNAL (deleteCollection(int)), this, SLOT (onDeleteCollection(int)));
  
  view ()->setProperty ("collectionsModel", QVariant::fromValue (_model));
}

void CollectionsViewController::onCollectionIndexChanged (int index)
{
  if (_backendView != nullptr)
    {
      _backendView->setParent (nullptr);
      _backendView->setParentItem (nullptr);
      _backendView->deleteLater ();
      _backendView = nullptr;
    }
  
  if (index >= 0 && index < _model->rowCount (QModelIndex ()))
    {
      const auto &coll = _model->collection (index);
      auto name = QString::fromUtf8 (coll->get_backend ().c_str ());
      auto sub = app ()->uiBackend (name).modifyCollection (app (), coll->get_id ());
      _backendView = sub->view<QQuickItem> ();
      _backendIndex = index;
    
      sub->view ()->setParent (view ());
      _backendView->setParentItem (view ()->property ("backendView").value<QQuickItem*> ());
      QObject::connect (view (), SIGNAL (accepted()), sub, SLOT (onAccepting()));
      QObject::connect (view (), SIGNAL (rejected()), sub, SLOT (onRejecting()));
      QObject::connect (sub, SIGNAL (accepted()), this, SLOT (onModifyCollectionAccepted()));
      QObject::connect (sub, SIGNAL (rejected()), this, SLOT (onModifyCollectionRejected()));

      onModifyCollectionRejected ();
    }
}

void CollectionsViewController::onCreateCollection ()
{
  QQmlComponent component (app ()->engine (), QUrl (QStringLiteral ("qrc:/createcoll.qml")));
  QQuickWindow *window = qobject_cast<QQuickWindow*> (component.create ());
  window->setTransientParent (view<QQuickWindow>());
  window->setModality (Qt::WindowModality::WindowModal);
  window->setFlag (Qt::WindowType::Dialog, true);

  auto sub = new CreateCollectionViewController (app (), window, window);

  _newCollWindow = window;
  QObject::connect (sub, SIGNAL (accepted(DbId)), this, SLOT (onNewCollectionAccepted(DbId)));
  QObject::connect (sub, SIGNAL (rejected()), this, SLOT (onNewCollectionRejected()));
  
  window->show ();
}

void CollectionsViewController::onDeleteCollection (int index)
{
  if (index >= 0 && index < _model->rowCount (QModelIndex ()))
    {
      app ()->backends ()->delete_collection (app ()->runtime (), app ()->db (), _model->collection (index));
      _model->reload ();
    }
}

void CollectionsViewController::onNewCollectionAccepted (DbId)
{
  onNewCollectionRejected ();
  
  _model->reload ();
}

void CollectionsViewController::onNewCollectionRejected ()
{
  if (_newCollWindow != nullptr)
    {
      _newCollWindow->deleteLater ();
      _newCollWindow = nullptr;
    }
}

void CollectionsViewController::onModifyCollectionAccepted ()
{
  if (_backendView != nullptr)
    {
      auto &coll = _model->collection (_backendIndex);
      coll->set_name (view ()->property ("collectionName").toString ().toUtf8 ().data ());
      coll->save (app ()->runtime (), app ()->db ());
    
      _model->reloadItem (_backendIndex);
    }
}

void CollectionsViewController::onModifyCollectionRejected ()
{
  if (_backendView != nullptr)
    {
      const auto &coll = _model->collection (_backendIndex);
      view ()->setProperty ("collectionName", QString::fromUtf8 (coll->get_name ().c_str ()));
    }
}
