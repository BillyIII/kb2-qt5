#ifndef COLLECTIONSVIEWCONTROLLER_HPP
#define COLLECTIONSVIEWCONTROLLER_HPP

#include <memory>

#include <QObject>
#include <QQuickWindow>
#include <QQuickItem>

#include "ViewController.hpp"
#include "CollectionsModel.hpp"


class CollectionsViewController : public ViewController
{
  Q_OBJECT
  
public:
  CollectionsViewController (const std::shared_ptr<App> &app, CollectionsModel *model, QObject *view,
                             QObject *parent = nullptr);

private slots:
  void onCollectionIndexChanged (int index);
  void onCreateCollection ();
  void onDeleteCollection (int index);
  void onNewCollectionAccepted (DbId id);
  void onNewCollectionRejected ();
  void onModifyCollectionAccepted ();
  void onModifyCollectionRejected ();
  
private:
  CollectionsModel *_model;
  QQuickWindow *_newCollWindow = nullptr;
  QQuickItem *_backendView = nullptr;
  int _backendIndex;
};

#endif // COLLECTIONSVIEWCONTROLLER_HPP
