import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

Window {
    id: root
    width: 640
    height: 480
    title: qsTr("Collection", "coll. window title")

    property alias collectionsModel: collections.model
    property alias backendView: backendView
    property alias collectionName: collectionName.text

    signal collectionIndexChanged(int index)
    signal createCollection()
    signal deleteCollection(int index)
    signal accepted()
    signal rejected()

    Component {
        id: collectionDelegate
        Rectangle {
            id: collectionRoot
            width: column.width
            height: column.height
            color: ListView.isCurrentItem ? "grey" : "transparent"

            required property string name
            required property int index
            required property var model

            Column {
                id: column
                width: collectionRoot.ListView.view.width
                clip: true

                Text { id: nameField; width: parent.width; text: collectionRoot.name }
            }

            MouseArea {
                anchors.fill: collectionRoot
                onClicked: collectionRoot.ListView.view.currentIndex = collectionRoot.index
            }
        }
    }

    SplitView {
        anchors.fill: parent

        ColumnLayout {
            SplitView.preferredWidth: 200

            ToolBar {
                Layout.fillWidth: true

                RowLayout {
                    anchors.fill: parent

                    ToolButton { text: qsTr("Add", "coll. window - add button"); onClicked: root.createCollection() }
                    ToolButton { text: qsTr("Delete", "coll. window - delete button"); onClicked: root.deleteCollection(collections.currentIndex) }
                }
            }

            ListView {
                id: collections
                Layout.fillWidth: true
                Layout.fillHeight: true
                delegate: collectionDelegate
                onCurrentIndexChanged: root.collectionIndexChanged(collections.currentIndex)
                onCurrentItemChanged: {
                    editor.enabled = collections.currentItem !== null;
                }
            }
        }

        ColumnLayout {
            id: editor
            enabled: false
            clip: true
            SplitView.fillWidth: true
            height: parent.height

            TabBar {
                id: collectionTabsBar
                Layout.fillWidth: true

                TabButton {
                    text: qsTr("Basic", "coll. window - basic tab")
                }
                TabButton {
                    text: qsTr("Details", "coll. window - details button")
                }
            }

            StackLayout {
                id: collectionTabsStack
                Layout.fillWidth: true
                Layout.fillHeight: true
                currentIndex: collectionTabsBar.currentIndex

                GridLayout {
                    columns: 2
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Text { text: qsTr("Name", "coll. window - basic - name") }
                    TextInput { id: collectionName; Layout.fillWidth: true }
                }

                Item {
                    id: backendView
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }

            RowLayout {
                Layout.fillWidth: true
                layoutDirection: Qt.RightToLeft

                Button { text: qsTr("Revert", "coll. window - revert button"); onClicked: root.rejected() }
                Button { text: qsTr("Apply", "coll. window - apply button"); onClicked: root.accepted() }
            }
        }
    }
}
