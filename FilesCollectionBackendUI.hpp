#ifndef FILESCOLLECTIONBACKENDUI_HPP
#define FILESCOLLECTIONBACKENDUI_HPP

#include "ViewController.hpp"


class FilesCollectionBackendUI : public CollectionBackendUI
{
public:
  FilesCollectionBackendUI ();

  virtual ViewController* createCollection (const std::shared_ptr<class App> &app) override;
  virtual ViewController* modifyCollection (const std::shared_ptr<class App> &app, DbId id) override;
  virtual ViewController* itemBar (const std::shared_ptr<class App> &app, DbId id) override;
};


class NewFilesCollectionController : public ViewController
{
  Q_OBJECT

public:
  NewFilesCollectionController (const std::shared_ptr<App> &app, QObject *view, QObject *parent = nullptr);

signals:
  void accepted (DbId id);
  void rejected ();

public slots:
  void onAccepting ();
  void onRejecting ();
};


class EditFilesCollectionController : public ViewController
{
  Q_OBJECT

public:
  EditFilesCollectionController (DbId id, const std::shared_ptr<App> &app, QObject *view, QObject *parent = nullptr);

signals:
  void accepted ();
  void rejected ();

public slots:
  void onAccepting ();
  void onRejecting ();
  
private:
  rust::Box<kb2::FilesCollection> _coll;

  void save ();
  void revert ();
};


class ItemBarController : public ViewController
{
  Q_OBJECT

public:
  ItemBarController (DbId id, const std::shared_ptr<App> &app, QObject *view, QObject *parent = nullptr);

private slots:
  void onOpen ();
  void onMailcap ();
  
private:
  DbId _id;
};

#endif // FILESCOLLECTIONBACKENDUI_HPP
