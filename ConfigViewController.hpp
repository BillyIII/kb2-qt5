#ifndef CONFIGVIEWCONTROLLER_HPP
#define CONFIGVIEWCONTROLLER_HPP

#include "ViewController.hpp"


class ConfigViewController : public ViewController
{
  Q_OBJECT

public:
  ConfigViewController (const std::shared_ptr<App> &app, QObject *view,
                        QObject *parent = nullptr);
};

#endif // CONFIGVIEWCONTROLLER_HPP
