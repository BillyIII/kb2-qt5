<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>collections</name>
    <message>
        <location filename="collections.qml" line="10"/>
        <source>Collection</source>
        <comment>coll. window title</comment>
        <translation>Collections</translation>
    </message>
    <message>
        <location filename="collections.qml" line="61"/>
        <source>Add</source>
        <comment>coll. window - add button</comment>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="collections.qml" line="62"/>
        <source>Delete</source>
        <comment>coll. window - delete button</comment>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="collections.qml" line="90"/>
        <source>Basic</source>
        <comment>coll. window - basic tab</comment>
        <translation>Basic</translation>
    </message>
    <message>
        <location filename="collections.qml" line="93"/>
        <source>Details</source>
        <comment>coll. window - details button</comment>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="collections.qml" line="108"/>
        <source>Name</source>
        <comment>coll. window - basic - name</comment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="collections.qml" line="123"/>
        <source>Revert</source>
        <comment>coll. window - revert button</comment>
        <translation>Revert</translation>
    </message>
    <message>
        <location filename="collections.qml" line="124"/>
        <source>Apply</source>
        <comment>coll. window - apply button</comment>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>config</name>
    <message>
        <location filename="config.qml" line="13"/>
        <source>Options</source>
        <comment>config window title</comment>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="config.qml" line="48"/>
        <source>Style</source>
        <comment>config window - style</comment>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="config.qml" line="55"/>
        <source>Font</source>
        <comment>config window - font</comment>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="config.qml" line="65"/>
        <source>Choose</source>
        <comment>config window - font choose button</comment>
        <translation>Choose</translation>
    </message>
    <message>
        <location filename="config.qml" line="68"/>
        <source>Database</source>
        <comment>config window - database</comment>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="config.qml" line="78"/>
        <source>Browse</source>
        <comment>config window - database browes button</comment>
        <translation>Browse</translation>
    </message>
</context>
<context>
    <name>createcoll</name>
    <message>
        <location filename="createcoll.qml" line="10"/>
        <source>Create collection</source>
        <comment>new coll. window title</comment>
        <translation>New Collection</translation>
    </message>
    <message>
        <location filename="createcoll.qml" line="66"/>
        <source>Cancel</source>
        <comment>new coll. panel - cancel button</comment>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="createcoll.qml" line="67"/>
        <source>Ok</source>
        <comment>new coll. panel - ok button</comment>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>editfilescoll</name>
    <message>
        <location filename="editfilescoll.qml" line="32"/>
        <source>Path</source>
        <comment>edit files coll. panel - collection path</comment>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="editfilescoll.qml" line="40"/>
        <source>Browse</source>
        <comment>edit files coll. panel - collection path browse button</comment>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="editfilescoll.qml" line="43"/>
        <source>Max text size</source>
        <comment>edit files coll. panel - max text size</comment>
        <translation>Max text size</translation>
    </message>
    <message>
        <location filename="editfilescoll.qml" line="46"/>
        <source>Max content size</source>
        <comment>edit files coll. panel - max content size</comment>
        <translation>Max content size</translation>
    </message>
</context>
<context>
    <name>error</name>
    <message>
        <location filename="error.qml" line="8"/>
        <source>Error</source>
        <comment>error dialog - title</comment>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="error.qml" line="14"/>
        <source>Unknown error</source>
        <comment>error dialog - empty message</comment>
        <translation>Unknown error</translation>
    </message>
</context>
<context>
    <name>filesitembar</name>
    <message>
        <location filename="filesitembar.qml" line="14"/>
        <source>Open</source>
        <comment>files item bar - open button</comment>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="filesitembar.qml" line="15"/>
        <source>Mailcap</source>
        <comment>files item bar - mailcap button</comment>
        <translation>Mailcap</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="11"/>
        <source>kb2-qt5</source>
        <comment>main window title</comment>
        <translation>Kb2</translation>
    </message>
    <message>
        <location filename="main.qml" line="25"/>
        <source>Collections</source>
        <comment>main window collections button</comment>
        <translation>Collections</translation>
    </message>
    <message>
        <location filename="main.qml" line="26"/>
        <source>Update</source>
        <comment>main window update button</comment>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="main.qml" line="27"/>
        <source>Config</source>
        <comment>main window config button</comment>
        <translation>Config</translation>
    </message>
    <message>
        <location filename="main.qml" line="122"/>
        <source>Rank asc</source>
        <comment>main window rank asc sorting</comment>
        <translation>Rank asc</translation>
    </message>
    <message>
        <location filename="main.qml" line="123"/>
        <source>Rank desc</source>
        <comment>main window rank desc sorting</comment>
        <translation>Rank desc</translation>
    </message>
    <message>
        <location filename="main.qml" line="124"/>
        <source>Date asc</source>
        <comment>main window date asc sorting</comment>
        <translation>Date asc</translation>
    </message>
    <message>
        <location filename="main.qml" line="125"/>
        <source>Date desc</source>
        <comment>main window date decs sorting</comment>
        <translation>Date desc</translation>
    </message>
    <message>
        <location filename="main.qml" line="131"/>
        <source>Search</source>
        <comment>main window search button</comment>
        <translation>Search</translation>
    </message>
</context>
<context>
    <name>newfilescoll</name>
    <message>
        <location filename="newfilescoll.qml" line="31"/>
        <source>Name</source>
        <comment>new files coll. panel - collection name</comment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="newfilescoll.qml" line="32"/>
        <source>Unnamed</source>
        <comment>new files coll. panel - collection name</comment>
        <translation>Unnamed</translation>
    </message>
    <message>
        <location filename="newfilescoll.qml" line="34"/>
        <source>Path</source>
        <comment>new files coll. panel - collection path</comment>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="newfilescoll.qml" line="42"/>
        <source>Browse</source>
        <comment>new files coll. panel - collection path browse button</comment>
        <translation>Browse</translation>
    </message>
</context>
</TS>
