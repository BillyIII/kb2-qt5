
#include "CreateCollectionViewController.hpp"


CreateCollectionViewController::CreateCollectionViewController (const std::shared_ptr<App> &app_, QObject *view_,
                                                                QObject *parent)
  : ViewController (app_, view_, parent)
{
  QObject::connect (view (), SIGNAL (backendChanged(QString)), this, SLOT (onBackendChanged(QString)));

  QStringList backends;
  for (const auto &b : app ()->uiBackends ())
    {
      backends.push_back (b.first);
    }
  view ()->setProperty ("backendsModel", backends);
}

void CreateCollectionViewController::onBackendChanged (QString name)
{
  if (_backendView != nullptr)
    {
      _backendView->setParent (nullptr);
      _backendView->setParentItem (nullptr);
      _backendView->deleteLater ();
      _backendView = nullptr;
    }
  
  auto sub = app ()->uiBackend (name).createCollection (app ());
  _backendView = sub->view<QQuickItem> ();
    
  sub->view ()->setParent (view ());
  _backendView->setParentItem (view ()->property ("backendView").value<QQuickItem*> ());
  QObject::connect (view (), SIGNAL (accepted()), sub, SLOT (onAccepting()));
  QObject::connect (view (), SIGNAL (rejected()), sub, SLOT (onRejecting()));
  QObject::connect (sub, SIGNAL (accepted(DbId)), this, SLOT (onBackendAccepted(DbId)));
  QObject::connect (sub, SIGNAL (rejected()), this, SLOT (onBackendRejected()));
}

void CreateCollectionViewController::onBackendAccepted (DbId id)
{
  emit accepted (id);
}

void CreateCollectionViewController::onBackendRejected ()
{
  emit rejected ();
}
