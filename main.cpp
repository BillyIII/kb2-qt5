
#include "pch.hpp"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>
#include <QStandardPaths>

#include "CollectionItemsModel.hpp"
#include "MainViewController.hpp"
#include "Config.hpp"
#include "QmlUtils.hpp"


static bool tryLoadLocalization(QTranslator &trans, QString name)
{
  name.push_back (".qm");
  
  auto path = QStandardPaths::locate (QStandardPaths::AppLocalDataLocation, name);
  if (path.isEmpty ())
    {
      path = name;
    }
  
  return trans.load(path);
}

int main (int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute (Qt::AA_EnableHighDpiScaling);
#endif
      
  QTranslator translator;

  if (!tryLoadLocalization (translator, QLocale ().name ()))
    {
      tryLoadLocalization (translator, "default");
    }

  QApplication app (argc, argv);

  // Config must have the same thread affinity as the engine.
  Config::instance ()->setApplication (&app);

  app.installTranslator(&translator);

  QQmlApplicationEngine engine;
  
  qmlRegisterSingletonInstance ("Kb2", 1, 0, "Config", Config::instance ());
  qmlRegisterSingletonInstance ("Kb2", 1, 0, "Utils", new QmlUtils (nullptr));
  
  const QUrl url (QStringLiteral ("qrc:/main.qml"));
  QObject::connect (&engine, &QQmlApplicationEngine::objectCreated,
                    &app, [url, &engine] (QObject *obj, const QUrl &objUrl)
                    {
                      if (url == objUrl)
                        {
                          if (obj != nullptr)
                            {
                              auto app = std::make_shared<App> (&engine,
                                                                Config::instance ()->database ().toUtf8 ().data ());
                              
                              auto model = new CollectionItemsModel (app, obj);

                              new MainViewController (app, model, obj, obj);
                            }
                          else
                            {
                              QCoreApplication::exit (-1);
                            }
                        }
                    },
                    Qt::QueuedConnection);
  engine.load (url);

  return app.exec ();
}
