#ifndef VIEWCONTROLLER_HPP
#define VIEWCONTROLLER_HPP

#include <type_traits>

#include <QObject>
#include <QQuickItem>
#include <QQuickWindow>

#include "App.hpp"


class ViewController : public QObject
{
  Q_OBJECT
  
public:
  ViewController (const std::shared_ptr<App> &app, QObject *view,
                  QObject *parent = nullptr);

  const std::shared_ptr<App>& app ();
  
  template<typename T = QObject>
  T* view ();

  QQuickItem* item ();

protected:
  template<typename Fun>
  std::invoke_result_t<Fun> try_ (Fun &&fun);

  void displayError (const char *err);

private:
  std::shared_ptr<App> _app;
  QObject *_view;
};

#include "ViewController.ipp"

#endif // VIEWCONTROLLER_HPP
