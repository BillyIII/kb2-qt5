
#include "QmlUtils.hpp"


QmlUtils::QmlUtils (QObject *parent)
  : QObject (parent)
{
}

QUrl QmlUtils::urlFromLocalPath (QString path)
{
  return QUrl::fromLocalFile (path);
}

QString QmlUtils::localPathFromUrl (QUrl url)
{
  return url.toLocalFile ();
}
