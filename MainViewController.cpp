
#include <stdexcept>

#include <QQuickView>
#include <QQuickItem>
#include <QQmlComponent>

#include "MainViewController.hpp"
#include "CollectionsViewController.hpp"
#include "CollectionsModel.hpp"
#include "ConfigViewController.hpp"


MainViewController::MainViewController (const std::shared_ptr<App> &app_, CollectionItemsModel *model, QObject *view_,
                                        QObject *parent)
  : ViewController (app_, view_, parent)
  , _model (model)
{
  QObject::connect (view (), SIGNAL (search(QString,int)), this, SLOT (onSearch(QString,int)));
  QObject::connect (view (), SIGNAL (collections()), this, SLOT (onCollections()));
  QObject::connect (view (), SIGNAL (update()), this, SLOT (onUpdate()));
  QObject::connect (view (), SIGNAL (config()), this, SLOT (onConfig()));
  QObject::connect (view (), SIGNAL (itemChanged(int)), this, SLOT (onItemChanged(int)));
  
  view ()->setProperty ("itemsModel", QVariant::fromValue (_model));
}

void MainViewController::onSearch (QString query, int sorting)
{
  auto queryStr = query.toUtf8 ();
  auto items = kb2::find_items (app ()->runtime (), app ()->db(), app ()->tags (),
                                0, static_cast<::kb2::Sorting> (sorting), queryStr.data ());
  _model->setItems (std::move (items));
}

void MainViewController::onCollections ()
{
  QQmlComponent component (app ()->engine (), QUrl (QStringLiteral ("qrc:/collections.qml")));
  QQuickWindow *window = qobject_cast<QQuickWindow*> (component.create ());
  window->setTransientParent (view<QQuickWindow> ());
  window->setModality (Qt::WindowModality::WindowModal);
  window->setFlag (Qt::WindowType::Dialog, true);

  auto model = new CollectionsModel (app (), window);
  model->reload ();
  
  new CollectionsViewController (app (), model, window, window);
  
  window->show ();
}

void MainViewController::onItemChanged (int index)
{
  if (_backendItemBarView != nullptr)
    {
      _backendItemBarView->setParent (nullptr);
      _backendItemBarView->setParentItem (nullptr);
      _backendItemBarView->deleteLater ();
      _backendItemBarView = nullptr;
    }

  if (-1 != index)
    {  
      auto id = _model->getItem (index);

      auto item = kb2::load_collection_item (app ()->runtime (), app ()->db (), id);
      auto coll = kb2::load_collection (app ()->runtime (), app ()->db (), item->get_collection ());
      auto backend = QString::fromUtf8 (coll->get_backend ().c_str ());
  
      auto sub = app ()->uiBackend (backend).itemBar (app (), id);
      _backendItemBarView = sub->view<QQuickItem> ();
    
      sub->view ()->setParent (view ());
      _backendItemBarView->setParentItem (view ()->property ("backendItemBar").value<QQuickItem*> ());
      // QObject::connect (view (), SIGNAL (accepted()), sub, SLOT (onAccepting()));
      // QObject::connect (view (), SIGNAL (rejected()), sub, SLOT (onRejecting()));
      // QObject::connect (sub, SIGNAL (accepted(DbId)), this, SLOT (onBackendAccepted(DbId)));
      // QObject::connect (sub, SIGNAL (rejected()), this, SLOT (onBackendRejected()));
    }
}

void MainViewController::onUpdate ()
{
  view ()->setProperty ("updating", true);
  _updateThread = new CollectionsUpdateThread (app (), this);
  QObject::connect (_updateThread, SIGNAL (updateDone()), this, SLOT (onUpdateDone()));
  _updateThread->start ();
}

void MainViewController::onUpdateDone ()
{
  if (_updateThread != nullptr)
    {
      if (_updateThread->error ())
        {
          try_([this]{
            throw std::runtime_error (_updateThread->error ()->c_str ());
          });
        }
      
      _updateThread->deleteLater ();
      _updateThread = nullptr;

      _model->setItems (rust::Vec<DbId> ());
    }
  
  view ()->setProperty ("updating", false);
}


CollectionsUpdateThread::CollectionsUpdateThread (const std::shared_ptr<App> &app, QObject *parent)
  : QThread (parent)
  , _app (app)
{
}

void CollectionsUpdateThread::run ()
{
  try
    {
      for (const auto &id : kb2::get_all_collections (_app->runtime (), _app->db ()))
        {
          auto coll = kb2::load_collection (_app->runtime (), _app->db (), id);
          _app->backends ()->update_collection (_app->runtime (), _app->db (), _app->tags (), coll);
        }
    }
  catch (const std::exception &ex)
    {
      _error = ex.what();
    }
  catch (const char *what)
    {
      _error = what;
    }
  catch (...)
    {
      _error = "Update has failed";
    }

  emit updateDone ();
}

const std::optional<std::string> CollectionsUpdateThread::error () const
{
  return _error;
}

void MainViewController::onConfig ()
{
  QQmlComponent component (app ()->engine (), QUrl (QStringLiteral ("qrc:/config.qml")));
  QQuickWindow *window = qobject_cast<QQuickWindow*> (component.create ());
  window->setTransientParent (view<QQuickWindow> ());
  window->setModality (Qt::WindowModality::WindowModal);
  window->setFlag (Qt::WindowType::Dialog, true);

  new ConfigViewController (app (), window, window);

  window->show ();
}
