import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import Qt.labs.platform 1.1
import Kb2 1.0


Item {
    id: root
    anchors.fill: parent

    property alias name: nameText.text
    property alias path: pathText.text

    FolderDialog {
        id: pathDialog
        modality: Qt.WindowModal

        onVisibleChanged: pathDialog.currentFolder = Utils.urlFromLocalPath(pathText.text)
    }

    ScrollView {
        id: fieldsScroll
        anchors.fill: parent
        contentWidth: fieldsScroll.availableWidth

        GridLayout {
            columns: 2

            Text { text: qsTr("Name", "new files coll. panel - collection name") }
            TextInput { id: nameText; text: qsTr("Unnamed", "new files coll. panel - collection name"); Layout.fillWidth: true }

            Text { text: qsTr("Path", "new files coll. panel - collection path") }
            RowLayout {
                TextInput {
                    id: pathText
                    Layout.fillWidth: true
                    verticalAlignment: TextInput.AlignVCenter
                    text: Utils.localPathFromUrl(pathDialog.folder)
                }
                Button { text: qsTr("Browse", "new files coll. panel - collection path browse button"); onClicked: pathDialog.open() }
            }
        }
    }
}
