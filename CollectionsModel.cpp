
#include "CollectionsModel.hpp"


CollectionsModel::CollectionsModel (const std::shared_ptr<App> &app, QObject *parent)
  : QAbstractListModel (parent)
  , _app (app)
{
}

QHash<int, QByteArray> CollectionsModel::roleNames () const
{
    QHash<int, QByteArray> roles;
    roles[(int)Roles::Id] = "id";
    roles[(int)Roles::Name] = "name";
    
    return roles;
}

QVariant CollectionsModel::data (const QModelIndex &index, int role) const
{
  QVariant result;

  if (index.isValid () && static_cast<std::size_t> (index.row ()) < _colls.size ())
    {
      const auto &coll = _colls[index.row ()];
          
      switch (role)
        {
        case static_cast<int> (Roles::Id):
          result = coll->get_id ();
          break;
        case static_cast<int> (Roles::Name):
          result = QString::fromUtf8 (coll->get_name ().c_str ());
          break;
        default:
          // empty
          break;
        }
    }
    
  return result;
}

int CollectionsModel::rowCount (const QModelIndex &parent) const
{
  if (parent.isValid ())
    {
      return 0;
    }
  else
    {
      return static_cast<int> (_colls.size ());
    }  
}

void CollectionsModel::reload ()
{
  beginResetModel ();

  _colls.clear ();

  for (const auto &id : kb2::get_all_collections (_app->runtime (), _app->db ()))
    {
      _colls.emplace_back (kb2::load_collection (_app->runtime (), _app->db (), id));
    }

  endResetModel ();
}

void CollectionsModel::reloadItem (int index)
{
  auto id = _colls.at (index)->get_id ();
  _colls[index] = kb2::load_collection (_app->runtime (), _app->db (), id);
  auto mindex = createIndex (index, 0);
  emit dataChanged (mindex, mindex);
}

::rust::Box<kb2::Collection>& CollectionsModel::collection (int index)
{
  return _colls.at (index);
}
