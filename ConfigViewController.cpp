
#include <QQuickStyle>

#include "ConfigViewController.hpp"
#include "Config.hpp"


ConfigViewController::ConfigViewController (const std::shared_ptr<App> &app_, QObject *view_,
                                            QObject *parent)
  : ViewController (app_, view_, parent)
{
  view ()->setProperty ("stylesModel", QVariant::fromValue (QQuickStyle::availableStyles ()));
  view ()->setProperty ("currentStyle", QQuickStyle::availableStyles ().indexOf (Config::instance ()->style ()));
}
