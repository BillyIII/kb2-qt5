#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <atomic>
#include <memory>

#include <QObject>
#include <QApplication>
#include <QSettings>
#include <QFont>


class Config : public QObject
{
  Q_OBJECT

  Q_PROPERTY (QApplication* application READ application WRITE setApplication NOTIFY applicationChanged);
  Q_PROPERTY (QFont font READ font WRITE setFont NOTIFY fontChanged);
  Q_PROPERTY (QString style READ style WRITE setStyle NOTIFY styleChanged);
  Q_PROPERTY (QString database READ database WRITE setDatabase NOTIFY databaseChanged);
  
public:
  // Do not use during static de-initialization.
  static Config* instance ();

  QApplication* application ();
  void setApplication (QApplication *application);

  QFont font ();
  void setFont (QFont value);

  QString style ();
  void setStyle (QString value);

  QString database ();
  void setDatabase (QString value);
  
signals:
  void applicationChanged (QApplication *newValue);
  void fontChanged (QFont newValue);
  void styleChanged (QString newValue);
  void databaseChanged (QString newValue);
  
private:
  std::atomic<bool> _isInitialized = false;
  static std::atomic<Config*> _instance;
  
  std::unique_ptr<QSettings> _settings;
  std::atomic<QApplication*> _application = nullptr;

  Config ();
  ~Config ();

  static Config* create ();
  void init ();

  void updateAppFont ();
  void updateAppStyle ();
};

#endif // CONFIG_HPP
