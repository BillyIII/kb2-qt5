
#include <stdexcept>

#include <QQuickItem>
#include <QDesktopServices>
#include <QStandardPaths>

#include "FilesCollectionBackendUI.hpp"


FilesCollectionBackendUI::FilesCollectionBackendUI ()
{
}

ViewController* FilesCollectionBackendUI::createCollection (const std::shared_ptr<class App> &app)
{
  QQmlComponent component (app->engine (), QUrl (QStringLiteral ("qrc:/newfilescoll.qml")));
  auto item = qobject_cast<QQuickItem*> (component.create ());
  return new NewFilesCollectionController (app, item, item);
}

ViewController* FilesCollectionBackendUI::modifyCollection (const std::shared_ptr<class App> &app, DbId id)
{
  QQmlComponent component (app->engine (), QUrl (QStringLiteral ("qrc:/editfilescoll.qml")));
  auto item = qobject_cast<QQuickItem*> (component.create ());
  return new EditFilesCollectionController (id, app, item, item);
}

ViewController* FilesCollectionBackendUI::itemBar (const std::shared_ptr<class App> &app, DbId id)
{
  QQmlComponent component (app->engine (), QUrl (QStringLiteral ("qrc:/filesitembar.qml")));
  auto item = qobject_cast<QQuickItem*> (component.create ());
  return new ItemBarController (id, app, item, item);
}


NewFilesCollectionController::NewFilesCollectionController (const std::shared_ptr<App> &app, QObject *view_,
                                                            QObject *parent)
  : ViewController (app, view_, parent)
{
  view ()->setProperty ("path", QStandardPaths::writableLocation (QStandardPaths::HomeLocation));
}

void NewFilesCollectionController::onAccepting ()
{
  try_([this]{
    auto colls = kb2::create_file_collection (app ()->runtime (), app ()->db ());

    colls.coll->set_name (view ()->property ("name").toString ().toUtf8 ().data ());
    colls.coll->save (app ()->runtime (), app ()->db ());

    colls.fcoll->set_path (view ()->property ("path").toString ().toUtf8 ().data ());
    colls.fcoll->save (app ()->runtime (), app ()->db ());

    emit accepted (colls.coll->get_id ());
  });
}

void NewFilesCollectionController::onRejecting ()
{
  emit rejected ();
}


EditFilesCollectionController::EditFilesCollectionController (DbId id, const std::shared_ptr<App> &app, QObject *view_,
                                                              QObject *parent)
  : ViewController (app, view_, parent)
  , _coll (kb2::load_file_collection (app->runtime (), app->db (), id))
{
  revert ();
}

void EditFilesCollectionController::onAccepting ()
{
  save ();
  emit accepted ();
}

void EditFilesCollectionController::onRejecting ()
{
  revert ();
  emit rejected ();
}

void EditFilesCollectionController::save ()
{
  try_([this]{
    _coll->set_path (view ()->property ("path").toString ().toUtf8 ().data ());
    _coll->set_max_text_size (view ()->property ("maxTextSize").toLongLong ());
    _coll->set_max_content_size (view ()->property ("maxContentSize").toLongLong ());
    _coll->save (app ()->runtime (), app ()->db ());
  });
}

void EditFilesCollectionController::revert ()
{
  view ()->setProperty ("path", QString::fromUtf8 (_coll->get_path ().c_str ()));
  view ()->setProperty ("maxTextSize", QString::number (_coll->get_max_text_size ()));
  view ()->setProperty ("maxContentSize", QString::number (_coll->get_max_content_size ()));
}


ItemBarController::ItemBarController (DbId id, const std::shared_ptr<App> &app, QObject *view_,
                                      QObject *parent)
  : ViewController (app, view_, parent)
  , _id (id)
{
  QObject::connect (view (), SIGNAL (open()), this, SLOT (onOpen()));
  QObject::connect (view (), SIGNAL (mailcap()), this, SLOT (onMailcap()));
}

void ItemBarController::onOpen ()
{
  try_([this]{
    auto item = kb2::load_collection_item (app ()->runtime (), app ()->db (), _id);
    auto fitem = kb2::load_file_item (app ()->runtime (), app ()->db (), _id);
    auto fcoll = kb2::load_file_collection (app ()->runtime (), app ()->db (), item->get_collection ());
    auto path = QString::fromUtf8 (fcoll->get_path ().c_str ()) + "/" + QString::fromUtf8 (fitem->get_path ().c_str ());
    QDesktopServices::openUrl(QUrl::fromLocalFile(path));
  });
}

void ItemBarController::onMailcap ()
{
  try_([this]{
    auto item = kb2::load_collection_item (app ()->runtime (), app ()->db (), _id);
    auto fitem = kb2::load_file_item (app ()->runtime (), app ()->db (), _id);
    auto fcoll = kb2::load_file_collection (app ()->runtime (), app ()->db (), item->get_collection ());
    auto path = QString::fromUtf8 (fcoll->get_path ().c_str ()) + "/" + QString::fromUtf8 (fitem->get_path ().c_str ());
    kb2::open_with_mailcap(app ()->runtime (), fitem->get_mime(), path.toUtf8 ().data (), kb2::MailcapEntryMode::Gui);
  });
}
