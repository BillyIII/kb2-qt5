#ifndef PCH_HPP
#define PCH_HPP

#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include <QAbstractListModel>
#include <QDir>
#include <QQmlComponent>
#include <QQuickItem>
#include <QQuickStyle>
#include <QQuickView>
#include <QQuickWindow>
#include <QStandardPaths>
#include <QThread>
#include <QTranslator>
#include <QUrl>
#include <QWindow>

#endif // PCH_HPP
