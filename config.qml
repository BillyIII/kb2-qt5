import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import Qt.labs.platform 1.1
import Kb2 1.0


Window {
    id: root
    width: 640
    height: 480
    title: qsTr("Options", "config window title")

    property alias stylesModel: stylesList.model
    property alias currentStyle: stylesList.currentIndex


    FontDialog {
        id: fontDialog
        modality: Qt.WindowModal
        font: Config.font

        onAccepted: function () {
            Config.font = fontDialog.font
        }

        onVisibleChanged: fontDialog.currentFont = Config.font
    }

    FileDialog {
        id: databaseDialog
        modality: Qt.WindowModal
        file: Utils.urlFromLocalPath(Config.database)

        onAccepted: function () {
            Config.database = Utils.localPathFromUrl(databaseDialog.file)
        }

        onVisibleChanged: databaseDialog.currentFile = Utils.urlFromLocalPath(Config.database)
    }

    GridLayout {
        columns: 2
        Layout.fillWidth: true
        Layout.fillHeight: true

        Text { text: qsTr("Style", "config window - style")}
        ComboBox {
            id: stylesList;
            //currentValue: Config.style
            onActivated: Config.style = stylesList.currentValue
        }

        Text { text: qsTr("Font", "config window - font") }
        RowLayout {
            Layout.fillWidth: true

            Text {
                id: fontText
                Layout.fillWidth: true
                verticalAlignment: TextInput.AlignVCenter
                text: fontDialog.font.family + " " + fontDialog.font.styleName + " " + fontDialog.font.pointSize
            }
            Button { text: qsTr("Choose", "config window - font choose button"); onClicked: fontDialog.open() }
        }

        Text { text: qsTr("Database", "config window - database") }
        RowLayout {
            Layout.fillWidth: true

            Text {
                id: databaseText
                Layout.fillWidth: true
                verticalAlignment: TextInput.AlignVCenter
                text: Utils.localPathFromUrl(databaseDialog.file)
            }
            Button { text: qsTr("Browse", "config window - database browes button"); onClicked: databaseDialog.open() }
        }
    }
}
