
inline ViewController::ViewController (const std::shared_ptr<App> &app, QObject *view,
                                       QObject *parent)
  : QObject (parent)
  , _app (app)
  , _view (view)
{
}

inline const std::shared_ptr<App>& ViewController::app ()
{
  return _app;
}
  
template<typename T>
inline T* ViewController::view ()
{
  return qobject_cast<T*> (_view);
}

inline QQuickItem* ViewController::item ()
{
  if (auto item = view<QQuickItem> ())
    {
      return item;
    }
  else if (auto wnd = view<QQuickWindow> ())
    {
      return wnd->property ("contentItem").value<QQuickItem*> ();
    }
  else
    {
      return nullptr;
    }
}

template<typename Fun>
inline std::invoke_result_t<Fun> ViewController::try_ (Fun &&fun)
{
  try
    {
      return fun ();
    }
  catch (const std::exception &ex)
    {
      displayError (ex.what());
    }
  catch (const char *what)
    {
      displayError (what);
    }
  catch (...)
    {
      displayError (nullptr);
    }
}
