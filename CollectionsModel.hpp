#ifndef COLLECTIONSMODEL_HPP
#define COLLECTIONSMODEL_HPP

#include <vector>

#include <QAbstractListModel>

#include "App.hpp"


class CollectionsModel : public QAbstractListModel
{
  Q_OBJECT
  
public:
  enum class Roles : int
    {
      Id = Qt::UserRole,
      Name
    };
  
  CollectionsModel (const std::shared_ptr<App> &app, QObject *parent = nullptr);

  virtual QHash<int, QByteArray> roleNames () const override;
  virtual QVariant data (const QModelIndex &index, int role) const override;
  virtual int rowCount (const QModelIndex &parent) const override;

  void reload ();
  void reloadItem (int index);

  ::rust::Box<kb2::Collection>& collection (int index);

private:
  std::shared_ptr<App> _app;
  std::vector<::rust::Box<kb2::Collection>> _colls;
};

#endif // COLLECTIONSMODEL_HPP
