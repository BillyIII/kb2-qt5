
#include <QQuickStyle>
#include <QStandardPaths>
#include <QDir>

#include "Config.hpp"


const char* CFG_ORG = "kb2";
const char* CFG_DOMAIN = "kb2.local";
const char* CFG_APP = "kb2-qt5";

const char* CFG_DATABASE = "Database";
const char* CFG_FONT = "Font";
const char* CFG_STYLE = "Style";

const char* CFG_DEFAULT_DATABASE_NAME = "db.sqlite3";


std::atomic<Config*> Config::_instance = nullptr;


Config::Config ()
  : QObject (nullptr)
{
}

Config::~Config ()
{
  auto inst = _instance.load ();
  if (nullptr != inst)
    {
      if (_instance.compare_exchange_strong (inst, nullptr))
        {
          delete inst;
        }
    }
}

Config* Config::create ()
{
  Config *inst = nullptr;
  Config *newinst = new Config ();
  
  if(_instance.compare_exchange_strong (inst, newinst))
    {
      newinst->init ();
      newinst->_isInitialized = true;
      return newinst;
    }
  else
    {
      while (!inst->_isInitialized)
        {
          // Nothing.
        }
      
      return inst;
    }
}

Config* Config::instance ()
{
  auto inst = _instance.load ();
  
  if (nullptr == inst)
    {
      inst = create ();
    }

  return inst;
}

void Config::init ()
{
  QCoreApplication::setOrganizationName (CFG_ORG);
  QCoreApplication::setOrganizationDomain (CFG_DOMAIN);
  QCoreApplication::setApplicationName (CFG_APP);
  
  _settings.reset (new QSettings ());

  updateAppFont ();
  updateAppStyle ();
}


QApplication* Config::application ()
{
  return _application;
}

void Config::setApplication (QApplication *application)
{
  _application = application;
  updateAppFont ();
  emit applicationChanged (application);
}


QFont Config::font ()
{
  auto val = _settings->value (CFG_FONT);
  if (!val.canConvert<QFont> ())
    {
      auto app = _application.load ();
      if (nullptr != app)
        {
          val = app->font ();
        }
    }
  if (!val.isValid ())
    {
      val = QFont ();
    }
  return qvariant_cast<QFont> (val);
}

void Config::setFont (QFont value)
{
  _settings->setValue (CFG_FONT, value);
  // updateAppFont ();
  emit fontChanged (value);
}

void Config::updateAppFont ()
{
  auto app = _application.load ();
  if (nullptr != app)
    {
      app->setFont (font ());
    }
}


QString Config::style ()
{
  auto val = _settings->value (CFG_STYLE);
  if (!val.canConvert<QString> ())
    {
      val = QQuickStyle::name ();
    }
  return qvariant_cast<QString> (val);
}

void Config::setStyle (QString value)
{
  _settings->setValue (CFG_STYLE, value);
  // updateAppStyle ();
  emit styleChanged (value);
}

void Config::updateAppStyle ()
{
  QQuickStyle::setStyle (style ());
}


QString Config::database ()
{
  auto val = _settings->value (CFG_DATABASE);
  if (!val.canConvert<QString> ())
    {
      auto path = QStandardPaths::locate(QStandardPaths::AppConfigLocation, CFG_DEFAULT_DATABASE_NAME);
      if (path.isEmpty ())
        {
          path = QStandardPaths::writableLocation (QStandardPaths::AppConfigLocation);
          QDir dir (path);
          dir.mkpath (".");          
          path.append("/");
          path.append(CFG_DEFAULT_DATABASE_NAME);
        }
      val = path;
    }
  return qvariant_cast<QString> (val);
}

void Config::setDatabase (QString value)
{
  _settings->setValue (CFG_DATABASE, value);
  emit databaseChanged (value);
}
