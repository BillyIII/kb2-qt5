import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.3

MessageDialog {
    title: qsTr("Error", "error dialog - title")
    modality: "WindowModal"

    property string errorText

    onErrorTextChanged: {
        text = errorText ? errorText : qsTr("Unknown error", "error dialog - empty message")
    }

    onAccepted: {
        destroy()
    }
}
